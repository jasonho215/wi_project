import csv
import re
import unicodedata

class Labeller:
    'Read in emoji data and label the message'
    __flushFreq = 500
    # c = { -1, 0 ,1 }
    __class_num = 3
    # thersold value of occurrence
    __thersold = 5

    '''
    emoji_pattern = re.compile("["
            u"\U0001F600-\U0001F64F"  # emoticons
            u"\U0001F300-\U0001F5FF"  # symbols & pictographs
            u"\U0001F680-\U0001F6FF"  # transport & map symbols
            u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
            u"\U00002700-\U000027BF"  # Dingbat
            u"\U0000FE00-\U0000FE0F"  # Variation Selectors
    "]+", flags=re.UNICODE)
    '''
    emoji_pattern = re.compile("["
            u"\U0001F600-\U0001F64F" # emoticons
            u"\U00002702-\U000027B0" # Dingbats
            u"\U0001F680-\U0001F6F3" # Transport and map symbols
            u"\U000024C2-\U0001F251" # Enclosed characters
            u"\U0001F300-\U0001F5FF" # Other additional symbols
            u"\U0000FE00-\U0000FE0F" # Variation Selectors
            u"\U0001F1E0-\U0001F1FF" # flags (iOS)
            u"\U00002600-\U000026FF" # Miscellaneous Symbols
            u"\U0001F914"
            u"\U0001F918"
            u"\U0001F913"
            u"\U0001F910"
    "]+", flags=re.UNICODE)
    emojiLabel = dict()

    def __init__(self, posDataPath, negDataPath):
        if len(Labeller.emojiLabel) == 0:
            with open('data/emoji.csv', newline='', encoding="utf-8") as emojiFile:
                reader = csv.reader(emojiFile, delimiter=',')
                for row in reader:
                    emoji = row[0]
                    occurrence = int(row[2])
                    # Perform Laplace estimation if occurrence is smaller than thersold value
                    negValue = int(row[4]) if occurrence > self.__thersold else int(row[4]) + 1
                    neuValue = int(row[5]) if occurrence > self.__thersold else int(row[5]) + 1
                    posValue = int(row[6]) if occurrence > self.__thersold else int(row[6]) + 1
                    occurrence = occurrence if occurrence > self.__thersold else occurrence + self.__class_num

                    Labeller.emojiLabel[emoji] = (-1 * negValue + 0 * neuValue + 1 * posValue) / occurrence

        self.posDataPath = posDataPath
        self.negDataPath = negDataPath
        self.posData = []
        self.negData = []

    def labelDatum(self, datum):
        match = Labeller.emoji_pattern.search(datum)
        if match == None:
            return None, None
        else:
            match = match.group()
        weight = 0
        for emoji in match:
            if Labeller.emojiLabel.get(emoji) == None:
                continue
            score = Labeller.emojiLabel[emoji]
            weight = weight + score
        print("[ {0:=.9f} ] {1:s}".format(weight, datum))
        return weight / len(match), Labeller.emoji_pattern.sub('',datum)

    def addDatum(self, weight, datum):
        if weight == None or datum == None:
            return None
        if datum == "":
            return None

        if weight > 0:
            self.posData.append( self.word_split(datum) )
        else:
            self.negData.append( self.word_split(datum) )

        if len(self.posData) == self.__flushFreq or len(self.negData) == self.__flushFreq:
            self.flushRecords()

    def word_split(self, data):
        data_new = []
        '''
        for word in data.split(" "):
            word_filter = [i.lower() for i in word]
            data_new.append(word_filter)
        '''
        [ data_new.append(word.lower()) for word in data.split(' ') ]
        return data_new
    '''
    def readFiles(self):
        with open('data/positive-data.csv', 'rb') as myfile:
            reader = csv.reader(myfile, delimiter=',')
            for val in reader:
                posdata.append(val[0])

        with open('data/negative-data.csv', 'rb') as myfile:
            reader = csv.reader(myfile, delimiter=',')
            for val in reader:
                negdata.append(val[0])
    '''

    def flushRecords(self):
        with open(self.posDataPath, 'a+', newline='', encoding='utf-8') as csvfile:
            writer = csv.writer(csvfile, delimiter=' ',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
            for r in self.posData:
                writer.writerow(r)

        with open(self.negDataPath, 'a+', newline='', encoding='utf-8') as csvfile:
            writer = csv.writer(csvfile, delimiter=' ',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
            [writer.writerow(r) for r in self.negData]

        self.posData.clear()
        self.negData.clear()

def main():
    data = 'data/msg_out.txt'
    labeller = Labeller('data/pos_data.txt', 'data/neg_data.txt')
    with open(data, 'r', buffering=1, encoding="utf-8") as f:
        for row in f.readlines():
            if row != '':
                row = re.sub('^\s*|\n', '', row)
                row = re.sub('([.,!?()\'\"])', r' \1 ', row)
                row = re.sub('\s{2,}', ' ', row)
                weight, datum = labeller.labelDatum( datum=row )
                labeller.addDatum(weight, datum)

if __name__ == '__main__':
    main()