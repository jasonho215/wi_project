import facebook
import pprint
import requests
import langid
import os
import re
import sys

app_id = "217715905321401"
app_secret = "2d19f9a989256393cd4caa1800891790"
counter = 0
num_post = 100
freq = 50
writePath = "data/msg_out.txt"
tokenPath = "token.txt"

emoji_pattern = re.compile("["
        u"\U0001F600-\U0001F64F"  # emoticons
        u"\U0001F300-\U0001F5FF"  # symbols & pictographs
        u"\U0001F680-\U0001F6FF"  # transport & map symbols
        u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                           "]+", flags=re.UNICODE)
try :
    tokenFile = open(tokenPath, "r+")
    token = tokenFile.readline()
    print("Token: {0:s}".format(token))
    graph = facebook.GraphAPI(access_token=token)
    posts = graph.get_object('21785951839/posts?fields=comments{message,message_tags}')
except IOError:
    print('Error on opening the file.')
    os._exit(-1)
except facebook.GraphAPIError:
    long_live = graph.get_app_access_token(app_id, app_secret)
    print(long_live)
    tokenFile.write(long_live)
finally:
    tokenFile.close()
    posts = graph.get_object('21785951839/posts?fields=comments{message,message_tags}&limit(100)')


msgs = []
pp = pprint.PrettyPrinter(indent=2)
def add(comment):
    global counter
    message = comment.get(u'message')
    message_tags = comment.get(u'message_tags')

    # require comments that is written in english and containing emoji
    result = langid.classify(message)
    if (result[0] == 'en' and emoji_pattern.search(message) != None):
        if message_tags != None:
            deletedWordLength = 0
            for message_tag in message_tags:
                offset = message_tag.get(u'offset') - deletedWordLength
                length = message_tag.get(u'length')
                message = ''.join([ message[:offset], message[offset+length:] ])

        message = message.replace('\n', '')

        if re.search('[a-zA-Z]', message) == None:
            return
        msgs.append(message)
        counter += 1

    if (counter % freq == 0 and counter > 0):
        mode = "a+" if os.path.exists(writePath) else "w+"
        print ("===============> {0:6d} comments crawled <===============".format(counter))
        with open(writePath, mode, encoding="utf-8") as f:
            for msg in msgs[-50:]:
                # print("{0:5d} : {1:s}".format(msgs.index(msg) , msg))
                f.write(msg)
                f.write('\n')
            f.flush()
            f.close()
        msgs.clear()
# pp.pprint(posts)

while True:
    try:
        # Perform some action on each post in the collection we receive from
        # Facebook.
        for post in posts.get(u'data'):
            comments = post.get(u'comments')
            try :
                if comments == None:
                    continue
                while True:
                    [add(comment=comment) for comment in comments['data']]
                    if comments.get('paging') != None:
                        try:
                            comments = requests.get(comments['paging']['next']).json()
                        except KeyError as e:
                            break
                        except Error as e:
                            pp.pprint(comments['paging']['next'])
                            pp.pprint(e)
                    else:
                        break
            except KeyError as err:
                pp.pprint("Comments Key Error : {0}".format(err))
                pp.pprint(comments)
                continue
        # Attempt to make a request to the next page of data, if it exists.
        posts = requests.get(posts['paging']['next']).json()
    except facebook.GraphAPIError:
        tokenFile = open(tokenPath, "r+")
        long_live = facebook.extend_access_token(app_id=app_id, app_secret=app_secret)
        print(long_live)
        tokenFile.write(long_live['access_token'])
        tokenFile.close()
    except KeyError as e:
        # When there are no more pages (['paging']['next']), break from the
        # loop and end the script.
        # print("Unexpected error:", sys.exc_info())
        print("Post Key Error : {0}".format(e))
        print(posts)
        break
    except Exception:
        print ("Unexpected error:", sys.exc_info())
        raise