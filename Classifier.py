import nltk
import nltk.classify.util
from nltk.classify import NaiveBayesClassifier
from nltk.corpus import stopwords
from __future__ import print_function
from nltk.collocations import BigramCollocationFinder
from nltk.metrics import BigramAssocMeasures
from nltk.metrics import scores
import itertools
import collections
import random

# read data files

# Update the set of stopwords
stopset = set(stopwords.words('english')) - set(('over', 'under', 'below',
                                                 'more', 'most', 'no', 'not', 'only', 'such', 'few', 'so', 'too',
                                                 'very',
                                                 'just', 'any', 'once'))


def stopword_filtered_word_feats(words):
    return dict([(word, True) for word in words if word not in stopset])


def word_feats(words):
    return dict([(word, True) for word in words])


def bigram_word_feats(words, score_fn=BigramAssocMeasures.chi_sq, n=200):
    bigram_finder = BigramCollocationFinder.from_words(words)
    bigrams = bigram_finder.nbest(score_fn, n)
    """
    print words
    for ngram in itertools.chain(words, bigrams):
    if ngram not in stopset:
    print ngram
    exit()
    """
    return dict([(ngram, True) for ngram in itertools.chain(words, bigrams)])


def bigram_word_feats_stopwords(words, score_fn=BigramAssocMeasures.chi_sq, n=200):
    bigram_finder = BigramCollocationFinder.from_words(words)
    bigrams = bigram_finder.nbest(score_fn, n)
    """
    print words
    for ngram in itertools.chain(words, bigrams):
    if ngram not in stopset:
    print ngram
    exit()
    """
    return dict([(ngram, True) for ngram in itertools.chain(words, bigrams) if ngram not in stopset])


# bigram trainning
def evaluate_classifier(featx):
    negfeats = [(featx(f), 'neg') for f in word_split(negdata)]
    posfeats = [(featx(f), 'pos') for f in word_split(posdata)]

    negcutoff = len(negfeats) * 3 / 4
    poscutoff = len(posfeats) * 3 / 4
    trainfeats = negfeats[:negcutoff] + posfeats[:poscutoff]
    testfeats = negfeats[negcutoff:] + posfeats[poscutoff:]

    # using 3 classifiers
    classifier_list = ['nb', ]

    for cl in classifier_list:
        if cl == 'nb':
            classifierName = 'Naive Bayes'
            classifier = NaiveBayesClassifier.train(trainfeats)

    refsets = collections.defaultdict(set)
    testsets = collections.defaultdict(set)
    for i, (feats, label) in enumerate(testfeats):
        refsets[label].add(i)
    observed = classifier.classify(feats)
    testsets[observed].add(i)
    accuracy = nltk.classify.util.accuracy(classifier, testfeats)
    pos_precision = scores.precision(refsets['pos'], testsets['pos'])


pos_recall = scores.recall(refsets['pos'], testsets['pos'])
pos_fmeasure = scores.f_measure(refsets['pos'], testsets['pos'])
neg_precision = scores.precision(refsets['neg'], testsets['neg'])
neg_recall = scores.recall(refsets['neg'], testsets['neg'])
neg_fmeasure = scores.f_measure(refsets['neg'], testsets['neg'])

print('')
print('---------------------------------------')
print('SINGLE FOLD RESULT ' + '(' + classifierName + ')')
print('---------------------------------------')
print('accuracy:', accuracy)
print('precision', (pos_precision + neg_precision) / 2)
print('recall', (pos_recall + neg_recall) / 2)
print('f-measure', (pos_fmeasure + neg_fmeasure) / 2)

# classifier.show_most_informative_features()

print('')

## CROSS VALIDATION

trainfeats = negfeats + posfeats

# SHUFFLE TRAIN SET
# As in cross validation, the test chunk might have only negative or only
# positive data
random.shuffle(trainfeats)
n = 5  # 5-fold cross-validation

for cl in classifier_list:

    subset_size = len(trainfeats) / n
    accuracy = []
    pos_precision = []
    pos_recall = []
    neg_precision = []
    neg_recall = []
    pos_fmeasure = []
    neg_fmeasure = []
    cv_count = 1
    for i in range(n):
        testing_this_round = trainfeats[i * subset_size:][:subset_size]
        training_this_round = trainfeats[:i * subset_size] + trainfeats[(i + 1) * subset_size:]

        if cl == 'nb':
            classifierName = 'Naive Bayes'
            classifier = NaiveBayesClassifier.train(training_this_round)

        refsets = collections.defaultdict(set)
        testsets = collections.defaultdict(set)
        for i, (feats, label) in enumerate(testing_this_round):
            refsets[label].add(i)
            observed = classifier.classify(feats)
            testsets[observed].add(i)

        cv_accuracy = nltk.classify.util.accuracy(classifier, testing_this_round)
        cv_pos_precision = scores.precision(refsets['pos'], testsets['pos'])
        cv_pos_recall = scores.recall(refsets['pos'], testsets['pos'])
        cv_pos_fmeasure = scores.f_measure(refsets['pos'], testsets['pos'])
        cv_neg_precision = scores.precision(refsets['neg'], testsets['neg'])
        cv_neg_recall = scores.recall(refsets['neg'], testsets['neg'])
        cv_neg_fmeasure = scores.f_measure(refsets['neg'], testsets['neg'])

        accuracy.append(cv_accuracy)
        pos_precision.append(cv_pos_precision)
        pos_recall.append(cv_pos_recall)
        neg_precision.append(cv_neg_precision)
        neg_recall.append(cv_neg_recall)
        pos_fmeasure.append(cv_pos_fmeasure)
        neg_fmeasure.append(cv_neg_fmeasure)

        cv_count += 1

    print('---------------------------------------')
    print('N-FOLD CROSS VALIDATION RESULT ' + '(' + classifierName +
          ')')
    print('---------------------------------------')
    print('accuracy:', sum(accuracy) / n)
    print('precision', (sum(pos_precision) / n + sum(neg_precision) / n)
          / 2)
    print('recall', (sum(pos_recall) / n + sum(neg_recall) / n) / 2)
    print('f-measure', (sum(pos_fmeasure) / n + sum(neg_fmeasure) / n) /
          2)
    print('')
